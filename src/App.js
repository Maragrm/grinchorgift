import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/home/Home';
import NavBar from './components/navBar/NavBar';
import Footer from './components/footer/Footer';

//Other
import { Counter } from './features/counter/Counter';
import { v4 as uuid } from 'uuid'

//Styles
import './App.css';


export default function App() {
  return (
    <Router>
      <div className="App">
        <NavBar></NavBar>
        <Switch>
          <Route exact path="/home" component={Home} />
        </Switch>
      </div>
      <Footer></Footer>
    </Router>
  )
}

